@extends('layouts.master')

@section('chart')

    <div class="panel-header panel-header-lg">

    <canvas id="bigDashboardChart"></canvas>


    </div>

    @endsection

@section('content')
<style>
    
    .label{display:inline;padding:.2em .6em .3em;font-size:75%;font-weight:700;line-height:1;color:#fff;text-align:center;white-space:nowrap;vertical-align:baseline;border-radius:.25em}a.label:focus,a.label:hover{color:#fff;text-decoration:none;cursor:pointer}.label:empty{display:none}.btn .label{position:relative;top:-1px}.label-default{background-color:#777}.label-default[href]:focus,.label-default[href]:hover{background-color:#5e5e5e}.label-primary{background-color:#337ab7}.label-primary[href]:focus,.label-primary[href]:hover{background-color:#286090}.label-success{background-color:#5cb85c}.label-success[href]:focus,.label-success[href]:hover{background-color:#449d44}.label-info{background-color:#5bc0de}.label-info[href]:focus,.label-info[href]:hover{background-color:#31b0d5}.label-warning{background-color:#f0ad4e}.label-warning[href]:focus,.label-warning[href]:hover{background-color:#ec971f}.label-danger{background-color:#d9534f}.label-danger[href]:focus,.label-danger[href]:hover{background-color:#c9302c}.glyphicon{position:relative;top:1px;display:inline-block;font-family:'Glyphicons Halflings';font-style:normal;font-weight:400;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}.glyphicon-envelope:before{content:"\2709"}
    .divider{height:1px;margin:9px 0;overflow:hidden;background-color:#e5e5e5}.show{display:block!important}
   
    
</style>

    <div class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="card card-stats">
                    <div class="card-body">

                    <div class="row">
                  
                    

 
 <ul>
  <li class="dropdown">
   <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="tog"><span class="label label-pill label-danger count" style="border-radius:10px;"></span> <span class="glyphicon glyphicon-envelope" style="font-size:18px; "></span></a>
   <ul class="dropdown-menu" id="drop"></ul>
  </li>
 </ul>



                </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="statistics">
                                    <div class="info">
                                        <div class="icon icon-primary">
                                        <i class="now-ui-icons users_single-02"></i>
                                        </div>
                                        <h3 class="info-title">66</h3>
                                        <h6 class="stats-title">Riders</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="statistics">
                                    <div class="info">
                                        <div class="icon icon-success">
                                            <i class="now-ui-icons business_money-coins"></i>
                                        </div>
                                        <h3 class="info-title"><small>PKR</small>555</h3>
                                        <h6 class="stats-title">Today Revenue</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="statistics">
                                    <div class="info">
                                        <div class="icon icon-info">
                                            <i class="now-ui-icons users_single-02"></i>
                                        </div>
                                        <h3 class="info-title">555</h3>
                                        <h6 class="stats-title">Customers</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="statistics">
                                    <div class="info">
                                        <div class="icon icon-danger">
                                            <i class="now-ui-icons objects_support-17"></i>
                                        </div>
                                        <h3 class="info-title">554</h3>
                                        <h6 class="stats-title">Support Requests</h6>
                                    </div>
                                </div>
                                <input type="hidden" id="_token" value="{{csrf_token()}}">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        

    </div>

@endsection

@push('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>






@endpush




