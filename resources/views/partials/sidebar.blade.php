<div class="sidebar" data-color="green">
    <div class="sidebar-wrapper" id="sidebar-wrapper">



@if(auth()->check() && auth()->user()->user_type == 'admin'  )

<div class="user">
            <div class="photo">
                <img src="{{ asset('admin/img/james.jpg') }}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                    <span>
                        @if(Auth::check())
                        {{auth()->user()->name}}
                        @else

                        Admin User

                        @endif
                        <b class="caret"></b>
                    </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        <li>
                            <a href="">
                                <span class="sidebar-mini-icon">MP</span>
                                <span class="sidebar-normal">My Profile</span>
                            </a>
                        </li>
                        <!-- <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">EP</span>
                                <span class="sidebar-normal">Edit Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">S</span>
                                <span class="sidebar-normal">Settings</span>
                            </a>
                        </li>-->
                    </ul>
                </div>
            </div>
        </div>

        <ul class="nav">
           

            <li class="{{Route::currentRouteName()== 'vendor.home' ? 'active':''}}">


                <a href="">

                    <i class="now-ui-icons design_app"></i>

                    <p>Dashboard</p>
                </a>

            </li>





            <li>
                

                <a href="#users" data-toggle="collapse">

                    <i class="now-ui-icons fa fa-qrcode"></i>

                    <p>
                        Users <b class="caret"></b>
                    </p>
                </a>

                <div class="collapse" id="users">
                    <ul class="nav">

                        <li >
                            <a href="">
                                <span class="sidebar-mini-icon">L</span>
                                <span class="sidebar-normal"> List Users</span>
                            </a>
                        </li>

                        <li >
                            <a href="">
                                <span class="sidebar-mini-icon">A</span>
                                <span class="sidebar-normal"> Add Recruiter </span>
                            </a>
                        </li>


                    </ul>
                </div>


            </li>

           


 

            </ul>

@endif
    </div>
</div>
