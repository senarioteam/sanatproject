<?php

namespace Database\Seeders;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Super Admin',
            
            'email' => 'superadmin@gmail.com',
            'user_type' => 'admin',
            'password' => bcrypt('12345678'),
//            'confirmation_code' => md5(uniqid(mt_rand(), true)),
//            'confirmed' => true,
        ]);
    }
}
